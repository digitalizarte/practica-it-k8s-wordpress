#/bin/sh
# Verifica que se este ejecutando con sudo .
if [ "$(id -u)" != "0" ]; then
        echo "Ejecutar con elevación de permisos"
        echo "sudo ./uninstall"
        exit 1
fi

# storage
kubectl delete -f storage_volume.yaml,logs_volume.yaml

# db
# kubectl delete -f db/db.pvc.yaml,db/db.deployment.yaml
kubectl delete -f db/db.deployment.yaml

# phpmyadmin
kubectl delete -f phpmyadmin/phpmyadmin.deployment.yaml,phpmyadmin/phpmyadmin.service.yaml

# wordpress
# kubectl delete -f wordpress/wordpress.pvc.yaml,wordpress/wordpress.deployment.yaml,wordpress/wordpress.service.yaml
kubectl delete -f wordpress/wordpress.deployment.yaml,wordpress/wordpress.service.yaml

# ingress
kubectl delete -f ingress.yaml

# storage
kubectl delete -f pv-nfs