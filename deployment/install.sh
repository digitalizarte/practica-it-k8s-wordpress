#/bin/sh
# Verifica que se este ejecutando con sudo .
if [ "$(id -u)" != "0" ]; then
        echo "Ejecutar con elevación de permisos"
        echo "sudo ./install"
        exit 1
fi

# storage
kubectl apply -f storage_volume.yaml,logs_volume.yaml

# db
# kubectl apply -f db/db.pvc.yaml,db/db.deployment.yaml
kubectl apply -f db/db.deployment.yaml

# phpmyadmin
kubectl apply -f phpmyadmin/phpmyadmin.deployment.yaml,phpmyadmin/phpmyadmin.service.yaml

# wordpress
# kubectl apply -f wordpress/wordpress.pvc.yaml,wordpress/wordpress.deployment.yaml,wordpress/wordpress.service.yaml
kubectl apply -f wordpress/wordpress.deployment.yaml,wordpress/wordpress.service.yaml

# ingress
kubectl apply -f ingress.yaml
